# Changelog

All notable changes to this project will be documented in this file.

## [1.2.0](https://gitlab.com/biomedit/django-detailed-request-logging/-/releases/1%2E2%2E0) - 2021-08-02

[See all changes since the last release](https://gitlab.com/biomedit/django-detailed-request-logging/compare/1%2E1%2E0...1%2E2%2E0)

### ✨ Features

- Include client IP address in the log ([30bb6f3](https://gitlab.com/biomedit/django-detailed-request-logging/commit/30bb6f3ffd70317cd3bb7268597439aa7a36a85b)), Close #2

### 🐞 Bug Fixes

- Handle cases when resolver_match is None ([f2db935](https://gitlab.com/biomedit/django-detailed-request-logging/commit/f2db935a0e3ec597418e21338d006f4c2c14e55f)), Close #3

### 🧱 Build system and dependencies

- Move project to gitlab.com ([ee104a1](https://gitlab.com/biomedit/django-detailed-request-logging/commit/ee104a1ffa537731161e251f6d447c5ce3dd1298))
- **coverage:** Include only the app code in coverage ([57987ef](https://gitlab.com/biomedit/django-detailed-request-logging/commit/57987ef8ef01e17543f5d600bdb68ce55af9703b))
- Move .pylintrc to pyproject.toml ([cb107ab](https://gitlab.com/biomedit/django-detailed-request-logging/commit/cb107ab8e42c4b8cc78dba71be0fddcca2eb75d0))

### 👷 CI

- Add mypy ([85ef9a9](https://gitlab.com/biomedit/django-detailed-request-logging/commit/85ef9a93cde0f3387e3da3a7dd4a111c3b97360b))
- **gitlab:** Not run pylint on release ([d087b01](https://gitlab.com/biomedit/django-detailed-request-logging/commit/d087b014a25c8d76fa44aaad422c491568212a1d))

### 📝 Documentation

- **README:** Add badges ([db54f7b](https://gitlab.com/biomedit/django-detailed-request-logging/commit/db54f7b4fb636df3c196ac0d7f92e3cc670360f5))

## [1.1.0](https://gitlab.com/biomedit/django-detailed-request-logging/-/releases/1%2E1%2E0) - 2021-06-23

[See all changes since the last release](https://gitlab.com/biomedit/django-detailed-request-logging/compare/1%2E0%2E1...1%2E1%2E0)

### ✨ Features

- Add app name field to the log ([3e4b3af](https://gitlab.com/biomedit/django-detailed-request-logging/commit/3e4b3afb226dc8be2cf8a749fb7698f1b79fcc93))

### 👷 CI

- **gitlab:** Add publish to pypi on deploy ([c1b609d](https://gitlab.com/biomedit/django-detailed-request-logging/commit/c1b609d65dbbde55481f0346dc6ae64dc244e55c))

## [1.0.1](https://gitlab.com/biomedit/django-detailed-request-logging/-/releases/1%2E0%2E1) - 2021-06-03

[See all changes since the last release](https://gitlab.com/biomedit/django-detailed-request-logging/compare/1%2E0%2E0...1%2E0%2E1)

### 🧱 Build system and dependencies

- Remove requirements duplication ([227e3cf](https://gitlab.com/biomedit/django-detailed-request-logging/commit/227e3cfa9399781ffa1185980352144e7e7916d9))

### 👷 CI

- **gitlab:** Add linting dependencies ([8e0c6e4](https://gitlab.com/biomedit/django-detailed-request-logging/commit/8e0c6e4613520acbf3424e4152ed32b9856a21e4))
- **gitlab:** Remove dependency scanning ([19d25fc](https://gitlab.com/biomedit/django-detailed-request-logging/commit/19d25fc0d2945d7de28e5df82070f1d6677ed217))

### 📝 Documentation

- **README:** Change installation guide to use SSH with git ([b0b28c8](https://gitlab.com/biomedit/django-detailed-request-logging/commit/b0b28c8d18c0235dc3e22d9e3537abe283845123))
- **README:** Extend the installation instructions by explanations for `LOGGING_REQUEST_MIDDLEWARE` ([8776668](https://gitlab.com/biomedit/django-detailed-request-logging/commit/8776668363f3f5c45043f9b0bef5a5f717400f97))

### 🎨 Style

- **pylint:** Remove `pylint_django` dependency as it's not a full django app but just a middleware ([b214b2c](https://gitlab.com/biomedit/django-detailed-request-logging/commit/b214b2cc21f98e3cc08445e397741ea26a0ea94c))

### ✅ Test

- Add Django settings fixture ([3e12682](https://gitlab.com/biomedit/django-detailed-request-logging/commit/3e1268255894142942e4872efe72232e4d419962))

## 1.0.0 - 2021-05-28
