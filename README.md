# django-detailed-request-logging

[![pipeline status](https://gitlab.com/biomedit/django-detailed-request-logging/badges/main/pipeline.svg)](https://gitlab.com/biomedit/django-detailed-request-logging/-/commits/main)
[![coverage report](https://gitlab.com/biomedit/django-detailed-request-logging/badges/main/coverage.svg)](https://gitlab.com/biomedit/django-detailed-request-logging/-/commits/main)
[![Ruff](https://img.shields.io/endpoint?url=https://raw.githubusercontent.com/astral-sh/ruff/main/assets/badge/v2.json)](https://github.com/astral-sh/ruff)
[![python version](https://img.shields.io/pypi/pyversions/django-detailed-request-logging.svg)](https://pypi.org/project/django-detailed-request-logging)
[![license](https://img.shields.io/badge/License-LGPLv3-blue.svg)](https://www.gnu.org/licenses/lgpl-3.0)
[![latest version](https://img.shields.io/pypi/v/django-detailed-request-logging.svg)](https://pypi.org/project/django-detailed-request-logging)

## Installation

Install the `django-detailed-request-logging` package and add the following
entry to `MIDDLEWARE` inside of your Django `settings.py` file:

```python
MIDDLEWARE = [
    "django_detailed_request_logging.middleware.LoggingMiddleware",
]
```

Then, add a new entry `LOGGING_REQUEST_MIDDLEWARE` to your `settings.py` file,
changing the value of `apps` to the names of the apps you want to log requests
on and changing the value of `skip_methods` to include all HTTP methods
you do **NOT** want to get logged:

```python
LOGGING_REQUEST_MIDDLEWARE = {
    "apps": ("projects",),
    "skip_methods": ("OPTIONS",),
}
```
