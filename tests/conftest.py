from typing import Any
from django.conf import settings


def pytest_configure() -> Any:
    settings.configure(LOGGING_REQUEST_MIDDLEWARE={})
