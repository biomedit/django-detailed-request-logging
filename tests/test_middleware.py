import logging
from typing import Optional, Any
from unittest.mock import Mock, patch

import pytest
from django.conf import LazySettings
from django.test.client import RequestFactory

from django_detailed_request_logging.middleware import (
    NO_LOGGING_MARKER,
    get_ip_address,
    no_logging,
    LoggingMiddleware,
)


def test_no_logging_decorator() -> None:
    @no_logging
    def f() -> None:
        pass

    @no_logging
    class A:
        pass

    assert getattr(f, NO_LOGGING_MARKER) is True
    assert getattr(A, NO_LOGGING_MARKER) is True
    assert getattr(A(), NO_LOGGING_MARKER) is True


class TestLoggingMiddleware:
    @pytest.mark.parametrize("test_input", (b"a" * 10, b"a" * 10_100))
    def test_trim_body(self, test_input: bytes) -> None:
        middleware = LoggingMiddleware(Mock())

        expected_length = (
            middleware.max_body_length
            if len(test_input) > middleware.max_body_length
            else len(test_input)
        )
        assert len(middleware.trim_body(test_input)) == expected_length

    @pytest.mark.parametrize(
        "code, method, result",
        (
            (200, "GET", logging.DEBUG),
            (200, "HEAD", logging.DEBUG),
            (200, "OPTIONS", logging.DEBUG),
            (200, "TRACE", logging.INFO),
            (200, "PATCH", logging.INFO),
            (200, "PUT", logging.INFO),
            (200, "POST", logging.INFO),
            (200, "DELETE", logging.INFO),
            (200, "FOO", logging.INFO),
            (403, "GET", logging.WARNING),
            (500, "POST", logging.ERROR),
        ),
    )
    def test_get_log_level(self, code: int, method: str, result: int) -> None:
        assert LoggingMiddleware.get_log_level(code, method) == result

    @patch("django_detailed_request_logging.middleware.request_logger")
    def test_log_request(self, mock_logger: Any) -> None:
        app_name = "myapp"
        middleware = LoggingMiddleware(Mock())
        middleware.request_body_copy = b""
        mock_request = Mock()
        mock_response = Mock()
        mock_request.META = {k: "" for k in middleware.sensitive_headers}
        mock_request.method = "GET"
        mock_request.get_full_path.return_value = "/"
        mock_request.reason_phrase = "OK"
        mock_request.user.is_authenticated = False
        mock_request.resolver_match.app_name = app_name
        mock_response.status_code = 200
        mock_response.reason_phrase = "OK"
        # pylint: disable=protected-access
        middleware._log_request(mock_request, mock_response)
        mock_logger.log.assert_called_once()
        assert mock_logger.log.call_args[0][0] == logging.DEBUG
        assert mock_logger.log.call_args[0][1]["headers"] == (
            {k: "*{masked}*" for k in middleware.sensitive_headers}
        )
        assert mock_logger.log.call_args[0][1]["app"] == app_name


class TestLoggingMiddlewareSkipLogging:
    class MockDrfView:
        def __init__(self) -> None:
            self.cls = Mock()

    @staticmethod
    def mock_view_factory(view_type: str) -> Any:
        method = "get"

        def f() -> None:
            pass

        class ViewDrf:
            def __init__(self) -> None:
                self.cls = Mock()

        class ViewDjango:
            def __init__(self) -> None:
                self.view_class = Mock()

        if view_type.startswith("drf"):
            mock_drf = ViewDrf()
            if view_type == "drf_actions":
                setattr(mock_drf, "actions", {method: "retrieve"})
                mock_drf.cls.retrieve = f
            else:
                setattr(mock_drf.cls, method, f)
            return mock_drf

        if view_type == "django":
            mock_django = ViewDjango()
            setattr(mock_django.view_class, method, f)
            return mock_django

        return f

    def test_skip_method(self, settings: LazySettings) -> None:
        settings.LOGGING_REQUEST_MIDDLEWARE = {"skip_methods": ("OPTIONS",)}
        method = "options"
        assert (
            # pylint: disable=protected-access
            LoggingMiddleware(Mock())._should_skip_log(Mock(method=method))
        )

    # Always log 404
    def test_should_log_404(self) -> None:
        assert not (
            # pylint: disable=protected-access
            LoggingMiddleware(Mock())._should_skip_log(Mock(resolver_match=None))
        )

    # Log only for apps defined in .apps
    def test_should_skip_app(self) -> None:
        mock_request = Mock()
        mock_request.resolver_match.app_name = "foo"
        # pylint: disable=protected-access
        assert LoggingMiddleware(Mock())._should_skip_log(mock_request)

    def test_views(self) -> None:
        app_name = "foo"
        middleware = LoggingMiddleware(Mock())
        middleware.apps = (app_name,)
        mock_request = Mock()
        mock_request.method = "GET"
        mock_request.resolver_match.app_name = app_name

        def _check_logging(view: Any, skipped: bool) -> None:
            mock_request.resolver_match.func = view
            # pylint: disable=protected-access
            assert middleware._should_skip_log(mock_request) == skipped

        # function-based views
        mock_view = self.mock_view_factory("function")
        _check_logging(mock_view, False)
        # Skip logging on the function-based view
        setattr(mock_view, NO_LOGGING_MARKER, True)
        _check_logging(mock_view, True)

        # Django views
        mock_view = self.mock_view_factory("django")
        _check_logging(mock_view, False)
        # Skip logging on a class view
        setattr(mock_view.view_class, NO_LOGGING_MARKER, True)
        _check_logging(mock_view, True)
        # Skip logging on a class view method
        setattr(mock_view.view_class, NO_LOGGING_MARKER, False)
        setattr(mock_view.view_class.get, NO_LOGGING_MARKER, True)
        _check_logging(mock_view, True)

        # djangorestframework views
        mock_view = self.mock_view_factory("drf")
        _check_logging(mock_view, False)
        # Skip logging on a class view
        setattr(mock_view.cls, NO_LOGGING_MARKER, True)
        _check_logging(mock_view, True)
        # Skip logging on a class view method
        setattr(mock_view.cls, NO_LOGGING_MARKER, False)
        setattr(mock_view.cls.get, NO_LOGGING_MARKER, True)
        _check_logging(mock_view, True)

        # djangorestframework views with actions
        mock_view = self.mock_view_factory("drf_actions")
        _check_logging(mock_view, False)
        # Skip logging on a class view
        setattr(mock_view.cls, NO_LOGGING_MARKER, True)
        _check_logging(mock_view, True)
        # Skip logging on a class view method
        setattr(mock_view.cls, NO_LOGGING_MARKER, False)
        setattr(mock_view.cls.retrieve, NO_LOGGING_MARKER, True)
        _check_logging(mock_view, True)


@pytest.mark.parametrize(
    "ip_addr",
    (
        "",
        "127.0.0.1",
        "203.0.113.195",
        "2001:db8:85a3:8d3:1319:8a2e:370:7348",
    ),
)
def test_get_ip_address(rf: RequestFactory, ip_addr: str) -> None:
    request = rf.get("/", REMOTE_ADDR=ip_addr)
    assert get_ip_address(request) == (ip_addr, None)


@pytest.mark.parametrize(
    "client_ip, proxy_ip",
    (
        ("", None),
        ("2001:db8:85a3:8d3:1319:8a2e:370:7348", None),
        ("127.0.0.1", "1.2.3.4"),
        ("203.0.113.195", "192.168.1,4.3.2.1"),
        (
            "2001:db8:85a3:8d3:1319:8a2e:370:7348",
            "2001:db8:83a3:8d3:1019:8a2e:370:7348",
        ),
    ),
)
def test_get_ip_address_with_proxy(
    rf: RequestFactory, client_ip: str, proxy_ip: Optional[str]
) -> None:
    request = rf.get(
        "/", HTTP_X_FORWARDED_FOR=client_ip + (f",{proxy_ip}" if proxy_ip else "")
    )
    result = get_ip_address(request)
    expected = (
        (client_ip, proxy_ip) if client_ip else (request.META.get("REMOTE_ADDR"), None)
    )
    assert result == expected
