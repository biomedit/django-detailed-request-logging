[build-system]
requires = ["hatchling"]
build-backend = "hatchling.build"

[project]
name = "django-detailed-request-logging"
version = "1.2.0"
license = { text = "LGPL3" }
description = "Middleware to log requests in Django with more detailed information"
readme = "README.md"
authors = [
  { name = "Christian Ribeaud", email = "christian.ribeaud@karakun.com" },
  { name = "François Martin", email = "francois.martin@karakun.com" },
  { name = "Gerhard Bräunlich", email = "gerhard.braeunlich@id.ethz.ch" },
  { name = "Jarosław Surkont", email = "jaroslaw.surkont@unibas.ch" },
  { name = "Robin Engler", email = "robin.engler@sib.swiss" },
  { name = "Swen Vermeul", email = "swen@ethz.ch" },
]
requires-python = ">=3.10"
classifiers = [
  "Programming Language :: Python :: 3",
  "Programming Language :: Python :: 3.10",
  "Programming Language :: Python :: 3.11",
  "Programming Language :: Python :: 3.12",
  "Programming Language :: Python :: 3.13",
  "License :: OSI Approved :: GNU Lesser General Public License v3 (LGPLv3)",
  "Operating System :: OS Independent",
]
dependencies = [
  "Django>=5.0",
  "djangorestframework>=3.15",
  "python-json-logger>=2",
]

[project.urls]
Repository = "https://gitlab.com/biomedit/django-detailed-request-logging"

[project.optional-dependencies]
dev = [
  "bandit==1.8.3",
  "django-stubs==5.1.3",
  "djangorestframework-stubs==3.15.3",
  "mypy==1.15.0",
  "pytest-cov==6.0.0",
  "pytest-django==4.10.0",
  "pytest-randomly==3.16.0",
  "pytest-xdist==3.6.1",
  "pytest==8.3.5",
  "ruff==0.9.9",
  "tox==4.24.1",
]

[tool.hatch.build]
exclude = ["tests"]

[tool.coverage.run]
branch = true
source = ["django_detailed_request_logging"]

[tool.tox]
legacy_tox_ini = '''
[tox]
envlist = clean,py313,report
skipsdist = True

[testenv]
deps =
    .[dev]
depends =
    py313: clean
    report: py313
commands = pytest {posargs: --cov --cov-append --cov-report= .}

[testenv:report]
deps = coverage[toml]
skip_install = true
commands =
    coverage report

[testenv:clean]
deps = coverage[toml]
skip_install = true
commands = coverage erase
'''

[tool.mypy]
strict = true
files = ["django_detailed_request_logging", "tests"]

[tool.ruff]
# Enable the pycodestyle (`E`) and Pyflakes (`F`) default rules.
# Additionally, add flake8-unused-arguments (ARG)
lint.select = ["E", "F", "ARG"]
target-version = "py313"
