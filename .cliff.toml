[changelog]
header = """
# Changelog

All notable changes to this project will be documented in this file.
"""
# template for the changelog body
# https://tera.netlify.app/docs/#introduction
body = """
{% set repo_url = get_env(name="REPO_URL") %}
{% if version -%}
    ## [{{ version | split(pat="/") | last }}]({{ repo_url }}/-/releases/{{ version | urlencode_strict }}) - {{ timestamp | date(format="%Y-%m-%d") }}

    {% if previous -%}
    [See all changes since the last release]({{ repo_url }}/compare/{{ previous.version | urlencode_strict }}...{{ version | urlencode_strict }})
    {% endif -%}
{% else -%}
    ## [unreleased]
{% endif %}
{% if commits | filter(attribute="breaking", value=true) | length > 0 -%}
  ### ⚠ BREAKING CHANGES

  {% for commit in commits | filter(attribute="breaking", value=true) -%}
    - {{ commit.breaking_description }}
  {% endfor -%}
{% endif -%}
{% for group, commits in commits | group_by(attribute="group") %}
    ### {{ group | striptags | trim | upper_first }}
    {% for commit in commits %}
        - {% if commit.scope %}**{{ commit.scope }}:** {% endif %}\
          {{ commit.message | upper_first }} \
          ([{{ commit.id | truncate(length=7, end="") }}]({{ repo_url }}/commit/{{ commit.id }}))\
          {% for footer in commit.footers -%}
            {% if footer.separator is containing("#") -%}
              , {{ footer.token }}{{ footer.separator }}{{ footer.value }}\
            {% endif -%}
            {% if footer.breaking -%}
            , ⚠ {{ footer.token }}\
            {% endif -%}
          {% endfor -%}
    {% endfor %}
{% endfor %}
"""
# remove the leading and trailing whitespace from the template
trim = true
# changelog footer
footer = ""

[git]
# parse the commits based on https://www.conventionalcommits.org
conventional_commits = true
# filter out the commits that are not conventional
filter_unconventional = true
# process each line of a commit as an individual commit
split_commits = false
# regex for parsing and grouping commits
commit_parsers = [
  { message = "^feat", group = "<!-- 0 -->✨ Features" },
  { message = "^fix", group = "<!-- 1 -->🐞 Bug Fixes" },
  { message = "^perf", group = "<!-- 2 -->🚀 Performance" },
  { message = "^build", group = "<!-- 3 -->🧱 Build system and dependencies" },
  { message = "^ci", group = "<!-- 4 -->👷 CI" },
  { message = "^docs", group = "<!-- 5 -->📝 Documentation" },
  { message = "^refactor", group = "<!-- 6 -->🧹 Refactoring" },
  { message = "^style", group = "<!-- 7 -->🎨 Style" },
  { message = "^test", group = "<!-- 8 -->✅ Test" },
]
# protect breaking changes from being skipped due to matching a skipping commit_parser
protect_breaking_commits = true
# filter out the commits that are not matched by commit parsers
filter_commits = true
# sort the commits inside sections by oldest/newest order
sort_commits = "newest"
